package week2BootcampPractise;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * 
 * @author Tridev Kavitha Bala 
 * 1. Login to
 *         https://login.salesforce.com/?locale=in 2. Click on menu button from
 *         the Left corner 3. Click view All and click Service Console from App
 *         Launcher 4. Click on the drop down and select Refunds 5. Click on New
 *         6. Select Account name 7. Select Status as Canceled 8. Give Amount as
 *         50000 and select Referenced in Type 9. Select Processing Mode as
 *         External 10. Click Save Expected Result:successRefund "R-000000001"
 *         was created. Step
 *         Video:https://drive.google.com/file/d/14A6bQd3dWBoPr8RoDpWDXFzhVCIn6TGO/view?usp=sharing
 */
public class CreateNewRefund {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		//WebDriverWait wait = new WebDriverWait(driver, 20);
		// Login to https://login.salesforce.com/?locale=in
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		// 2. Click on menu button from the Left corner
		// 3. Click view All and click Service Console from App Launcher
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		// Click on Service Console
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Service Console");
		driver.findElement(By.xpath("//p[@class='slds-truncate']/mark")).click();
		// 4. Click on the drop down and select Refunds
		driver.findElement(By.xpath(
				"//button[@class='slds-button slds-button_icon slds-p-horizontal__xxx-small slds-button_icon-small slds-button_icon-container']/lightning-primitive-icon"))
				.click();
		driver.findElement(By.xpath("//span[text()='Refunds']")).click();
		// 5. Click on New
		Thread.sleep(3000);
		driver.findElement(By.xpath("(//div[text()='New'])[1]")).click();
		// 6. Select Account name
		driver.findElement(By.xpath("//input[@placeholder='Search Accounts...']")).click();
		driver.findElement(By.xpath("(//div[text()='Kavitha M'])[1]")).click();
		// 7. Select Status as Canceled
		driver.findElement(By.xpath("(//a[text()='--None--'])[1]")).click();
		driver.findElement(By.xpath("//a[@title='Canceled']")).click();
		// 8. Give Amount as 50000 and select Referenced in Type
		driver.findElement(By.xpath("//input[@class='input uiInputSmartNumber']")).sendKeys("50000");

		driver.findElement(By.xpath("(//a[text()='--None--'])[1]")).click();
		driver.findElement(By.xpath("//a[@title='Referenced']")).click();
		// 9. Select Processing Mode as External
		driver.findElement(By.xpath("(//a[text()='--None--'])[1]")).click();
		driver.findElement(By.xpath("//a[@title='External']")).click();
		// 10. Click Save
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//span[text()='Save'])[3]")).click();
		Thread.sleep(5000);
		// Expected Result:successRefund "R-000000001" was created
		WebElement toastElement = driver
				.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']"));
		String toastMessage = toastElement.getText();
		System.out.println("Toast Message after refund:" + toastMessage);
		if (toastMessage.contains("created")) {
			System.out.println("Refund success");
		} else {
			System.out.println("Refund is not success");
		}
		driver.close();
	}

}
