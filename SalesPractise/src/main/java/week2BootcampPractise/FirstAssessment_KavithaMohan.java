package week2BootcampPractise;

import java.time.Duration;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;
/**
 * 
 * @author Kavitha M
 * 1. Login to https://login.salesforce.com
2. Click on toggle menu button from the left corner
3. Click view All 
4. Click Service Console from App Launcher
5. Select Home from the DropDown
//6. Add CLOSED + OPEN values and result should set as the GOAL (If the result is less than 10000 then set the goal as 10000)
7. Select Dashboards from DropDown
8. Click on New Dashboard
9. Enter the Dashboard name as "YourName_Workout"
10. Enter Description as Testing and Click on Create
11. Click on Done
12. Verify the Dashboard is Created
13. Click on Subscribe
14. Select Frequency as "Daily"
15. Time as 10:00 AM
16. Click on Save
17. Verify "You started Dashboard Subscription" message displayed or not
18. Close the "YourName_Workout" tab
19. Click on Private Dashboards
20. Verify the newly created Dashboard available
21. Click on dropdown for the item
22. Select Delete
23. Confirm the Delete
24. Verify the item is not available under Private Dashboard folder
 *
 */
public class FirstAssessment_KavithaMohan {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		//	1. Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(50));
		WebDriverWait wait = new WebDriverWait(driver,20);
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		//2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		//3. Click View All 
		WebElement viewAllWebElement = driver.findElement(By.xpath("//button[text()='View All']"));
		wait.until(ExpectedConditions.visibilityOf(viewAllWebElement));
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		//4. Click Service Console from App Launcher
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Service Console");
		driver.findElement(By.xpath("//p[@class='slds-truncate']/mark")).click();
		//7. Select Dashboards from DropDown
		driver.findElement(By.xpath("//button[@class='slds-button slds-button_icon slds-p-horizontal__xxx-small slds-button_icon-small slds-button_icon-container']/lightning-primitive-icon")).click();
		driver.findElement(By.xpath("//span[text()='Dashboards']")).click();
		//8. Click on New Dashboard
		driver.findElement(By.xpath("//div[text()='New Dashboard']")).click();
		//9. Enter the Dashboard name as "YourName_Workout"
		//driver.switchTo().frame(0);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(driver.findElement(By.xpath("//iframe[@title='dashboard']"))));
		WebElement dashboardNameInputElement = driver.findElement(By.id("dashboardNameInput"));
		wait.until(ExpectedConditions.visibilityOf(dashboardNameInputElement));
		dashboardNameInputElement.sendKeys("Kavitha Mohan_Workout");
		String dashboardNameElement = driver.findElement(By.id("dashboardNameInput")).getText();
		//10. Enter Description as Testing and Click on Create
		driver.findElement(By.xpath("//input[@id='dashboardDescriptionInput']")).sendKeys("Testing");
		driver.findElement(By.xpath("//button[text()='Create']")).click();
		//11. Click on Done
		driver.switchTo().defaultContent();
		String titleOfPage1 = driver.getTitle();
		System.out.println("Title of the page1:"+titleOfPage1);
		Thread.sleep(10000);
		String titleOfPage2 = driver.getTitle();
		System.out.println("Title of the page2:"+titleOfPage2);
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
		wait.until(ExpectedConditions.titleContains(dashboardNameElement));
		WebElement doneElement = driver.findElement(By.xpath("//button[@class='slds-button doneEditing']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", doneElement);
		driver.switchTo().defaultContent();
		//12. Verify the Dashboard is Created
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
		WebElement textElement = driver.findElement(By.xpath("//span[@class='slds-page-header__title slds-truncate']"));
		String DashboardName = textElement.getText();
		System.out.println("Dashboard Actual Name:"+DashboardName);
		if(DashboardName.contains("Kavitha Mohan_Workout")) {
			System.out.println("Dashboard created successfully");
		}else {
			System.out.println("Dashboard is not created successfully");
		}
		//13. Click on Subscribe
		driver.findElement(By.xpath("//button[text()='Subscribe']")).click();
		driver.switchTo().defaultContent();
		//14. Select Frequency as "Daily"
		driver.findElement(By.xpath("//span[text()='Daily']")).click();
		//15. Time as 10:00 AM
		WebElement selectTimeElement = driver.findElement(By.xpath("//select[@id='time']"));
		Select dropDown = new Select(selectTimeElement);
		dropDown.selectByValue("10");
		//16. Click on Save
		driver.findElement(By.xpath("(//span[@class=' label bBody'])[4]")).click();
		Thread.sleep(3000);
		//17. Verify "You started Dashboard Subscription" message displayed or not
		WebElement toastElement = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']"));
		String toastMessage = toastElement.getText();
		System.out.println("Toast Message after subscription:"+toastMessage);
		if(toastMessage.contains("started a dashboard subscription")) {
			System.out.println("You started a Dashboard subscription message displayed");
		}else {
			System.out.println("You started a Dashboard subscription message is not displayed");
		}
		//18. Close the "YourName_Workout" tab
		driver.findElement(By.xpath("//button[@title='Close Kavitha Mohan_Workout']")).click();
		//19. Click on Private Dashboards
		driver.findElement(By.xpath("//button[@class='slds-button slds-button_icon slds-p-horizontal__xxx-small slds-button_icon-small slds-button_icon-container']/lightning-primitive-icon")).click();
		driver.findElement(By.xpath("//span[text()='Dashboards']")).click();
		driver.findElement(By.xpath("//a[text()='Private Dashboards']")).click();
		//20. Verify the newly created Dashboard available
		Thread.sleep(30000);
		String myWorkouttext = driver.findElement(By.xpath("//th[@data-label='Dashboard Name']//span//lightning-formatted-url/a[text()='Kavitha Mohan_Workout']")).getText();
		System.out.println("Workout Text:"+myWorkouttext);
		if(myWorkouttext.equals("Kavitha Mohan_Workout")) {
			System.out.println("Newly created dashboard available");
		}else {
			System.out.println("Newly created dashboard is not available");
		}
		//21. Click on dropdown for the item
		driver.findElement(By.xpath("(//button[@class='slds-button slds-button_icon-border slds-button_icon-x-small']/lightning-primitive-icon)[1]")).click();
		//22. Select Delete
		driver.findElement(By.xpath("//span[text()='Delete']")).click();
		//23. Confirm the Delete
		driver.findElement(By.xpath("(//span[text()='Delete'])[2]")).click();
		Thread.sleep(3000);
		WebElement toastElement2 = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']"));
		String toastMessage2 = toastElement2.getText();
		Thread.sleep(3000);
		System.out.println("Toast Message after deletion:"+toastMessage2);
		if(toastMessage2.contains("deleted")) {
			System.out.println("Dashboard created deleted successfully");
		}else {
			System.out.println("Dashboard created is not deleted successfully");
		}
		driver.close();
	}

	
}
