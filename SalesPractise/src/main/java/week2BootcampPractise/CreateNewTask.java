package week2BootcampPractise;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * 
 * @author Tridev Kavitha Bala
 *1. Login to https://login.salesforce.com
2. Click on toggle menu button from the left corner
3. Click view All from App Launcher
4. Click on Content tab 
5. Click View All from Today's Task
6. Click on Show one more Action and click New Task
7. Select a Account Name in Assigned to field 
8. Select a subject as Email
9. Set Priority as High and Status as In Progress
10. Click on the image of Name field, click on Contacts and select Contact
11. Click on the image of Related To field, click on Product and Select Product
12. Click Save
Expected Result:
Task Send Quote should br created with the given Details.
Step Video:
https://drive.google.com/file/d/1hz3ZC1ZDMCCIfd3tfWGNITRWqioFPJcQ/view?usp=sharing
 */
public class CreateNewTask {

	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		// Login to https://login.salesforce.com/?locale=in
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		// 2. Click on menu button from the Left corner
		// 3. Click view All and click Service Console from App Launcher
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		//4. Click on Content tab 
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Content");
		driver.findElement(By.xpath("//p[@class='slds-truncate']/mark")).click();
		//5. Click View All from Today's Task
		driver.findElement(By.xpath("(//span[text()='View All'])[2]")).click();
		//6. Click on Show one more Action and click New Task
		driver.findElement(By.xpath("//a[@title='Show one more action']")).click();
		driver.findElement(By.xpath("//a[@title='New Task']")).click();
		//7. Select a Account Name in Assigned to field 
		driver.findElement(By.xpath("//input[@class='default input uiInput uiInputTextForAutocomplete uiInput--default uiInput--input uiInput uiAutocomplete uiInput--default uiInput--lookup']")).click();
		WebElement accountNameElement = driver.findElement(By.xpath("//span[text()='Derrick Dsouza']"));
		wait.until(ExpectedConditions.visibilityOf(accountNameElement));
		wait.until(ExpectedConditions.elementToBeClickable(accountNameElement));
		driver.findElement(By.xpath("//span[text()='Derrick Dsouza']")).click();
		//8. Select a subject as Email
		driver.findElement(By.xpath("//input[@class='slds-combobox__input slds-input']")).click();
		WebElement subjectElement = driver.findElement(By.xpath("//input[@class='slds-combobox__input slds-input']"));
		wait.until(ExpectedConditions.elementToBeClickable(subjectElement));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click()", subjectElement);
		driver.findElement(By.xpath("//lightning-base-combobox-item[@data-value='Email']")).click();
		//9. Set Priority as High and Status as In Progress
		WebElement priorityElement = driver.findElement(By.xpath("(//a[@class='select'])[2]"));
		wait.until(ExpectedConditions.elementToBeClickable(priorityElement));
		driver.findElement(By.xpath("(//a[@class='select'])[2]")).click();
		WebElement priorityHighElement = driver.findElement(By.xpath("//a[@title='High']"));
		wait.until(ExpectedConditions.elementToBeClickable(priorityHighElement));
		driver.findElement(By.xpath("//a[@title='High']")).click();
		driver.findElement(By.xpath("(//a[@class='select'])[1]")).click();
		WebElement statusElement = driver.findElement(By.xpath("//a[@title='In Progress']"));
		wait.until(ExpectedConditions.elementToBeClickable(statusElement));
		driver.findElement(By.xpath("//a[@title='In Progress']")).click();
		//10. Click on the image of Name field, click on Contacts and select Contact
		WebElement nameElement = driver.findElement(By.xpath("(//a[@class='entityMenuTrigger slds-button slds-button--icon slds-shrink-none slds-m-vertical--xxx-small slds-grid slds-grid_align-center'])[2]"));
		wait.until(ExpectedConditions.elementToBeClickable(nameElement));
		driver.findElement(By.xpath("(//a[@class='entityMenuTrigger slds-button slds-button--icon slds-shrink-none slds-m-vertical--xxx-small slds-grid slds-grid_align-center'])[2]")).click();
		WebElement contImgElement = driver.findElement(By.xpath("//img[@title='Contacts']"));
		wait.until(ExpectedConditions.elementToBeClickable(contImgElement));
		driver.findElement(By.xpath("//img[@title='Contacts']")).click();		
		driver.findElement(By.xpath("//input[@title='Search Contacts']")).click();
		driver.findElement(By.xpath("(//div[@class='primaryLabel slds-truncate slds-lookup__result-text'])[1]")).click();
		//11. Click on the image of Related To field, click on Product and Select Product
		driver.findElement(By.xpath("(//a[@class='entityMenuTrigger slds-button slds-button--icon slds-shrink-none slds-m-vertical--xxx-small slds-grid slds-grid_align-center'])[3]")).click();
		driver.findElement(By.xpath("//span[@title='Products']")).click();
		driver.findElement(By.xpath("//input[@title='Search Products']")).click();
		driver.findElement(By.xpath("(//li[@class='lookup__item  default uiAutocompleteOption forceSearchInputLookupDesktopOption']/a/div[2]/div)[11]")).click();
		//12. Click Save
		driver.findElement(By.xpath("//button[@title='Save']")).click();
	}

}
