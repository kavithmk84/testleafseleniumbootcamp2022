package week2BootcampPractise;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * 
 * @author Tridev Kavitha Bala 1) Launch the app 2) Click Login 3) Login with
 *         the credentials 4) Click on the App Laucher Icon left to Setup 5)
 *         Click on View All 6) Click on Service Appointments 7) Click on Show
 *         more Option for the recently created Appoinments 8) Select Option
 *         Cancel service Appointment 9) click on Confirm Cancellation 10)
 *         Verify the status of The Appointment
 * 
 *         Expected Result:The Status of the appointment should be verified
 *
 */
public class CancelServiceAppointment {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		// 1) Launch the app 2) Click Login
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		// 3) Login with the credentials
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		// 4) Click on the App Launcher Icon left to Setup
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		// 5) Click on View All
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		// 6) Click on Service Appointments
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']"))
				.sendKeys("Service appointments");
		driver.findElement(By.xpath("//p[@class='slds-truncate']/mark")).click();
		// 7) Click on Show more Option for the recently created Appoinments
		driver.findElement(By.xpath("//button[@title='Select a List View']")).click();
		driver.findElement(By.xpath("//span[text()='Recently Viewed']")).click();
		Thread.sleep(5000);
		// 8) Select Option Cancel service Appointment
		driver.findElement(By.xpath("(//span[@class='slds-icon_container slds-icon-utility-down']/span)[1]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[@title='Cancel Service Appointment']")).click();
		// 9) click on Confirm Cancellation
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()='Confirm Cancellation']")).click();
		// 10) Verify the status of The Appointment
		Thread.sleep(3000);
		WebElement toastElement = driver
				.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']"));
		String toastMessage = toastElement.getText();
		System.out.println("Toast Message after deletion:" + toastMessage);
		if (toastMessage.contains("saved")) {
			System.out.println("Appointment was deleted");
		} else {
			System.out.println("Appointment was not deleted");
		}

	}

}
