package week2BootcampPractise;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * 
 * @author Tridev Kavitha Bala 1) Launch the app 2) Click Login 3) Login with
 *         the credentials 4) Click on the App Launcher Icon left to Setup 5)
 *         Click on View All 6) Click on Service Appointments 7) Click on New 8)
 *         Enter Description as Creating Service Appointments 9) Click on Search
 *         Accounts under Parent Record 10) Click on New Account 11) Enter Your
 *         name in the Account Name 12) Click On Save 13) Verify Parent Record
 *         14) Select Today date in Earliest Start Permitted and Nearest Current
 *         Time 15) Select 5+ days from Today's date as Due Date 16) click on
 *         save 17) Get the Appointment Number 18.Click on the App Launcher Icon
 *         left to Setup 19. Click on View All 20. Click on Service Appointments
 *         21. Search the Appoinment Number and Verify Expected Result:The
 *         Appointment number should be verified
 * 
 *
 */
public class CreateNewServiceAppointment {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		// 1) Launch the app 2) Click Login
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		// 3) Login with the credentials
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		// 4) Click on the App Launcher Icon left to Setup
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		// 5) Click on View All
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		// 6) Click on Service Appointments
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']"))
				.sendKeys("Service appointments");
		driver.findElement(By.xpath("//p[@class='slds-truncate']/mark")).click();
		// 7. Click on New
		driver.findElement(By.xpath("//div[text()='New']")).click();
		// 8) Enter Description as Creating Service Appointments
		driver.findElement(By.xpath("//textarea[@role='textbox']")).sendKeys("Creating Service Appointments");
		// 9) Click on Search Accounts under Parent Record
		driver.findElement(By.xpath("//input[@title='Search Accounts']")).click();
		// 10) Click on New Account
		driver.findElement(By.xpath("//span[text()='New Account']")).click();
		// 11) Enter Your name in the Account Name
		driver.findElement(By.xpath("(//div[@class='uiInput uiInputText uiInput--default uiInput--input']/input)[9]"))
				.sendKeys("Kavitha M");
		// 12) Click On Save
		driver.findElement(By.xpath("(//button[@title='Save'])[2]")).click();
		// 13) Verify Parent Record
		String toastMessage = driver
				.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']"))
				.getText();
		// String parentRecordElement =
		// driver.findElement(By.xpath("(//span[@class='pillText'])[1]")).getText();
		System.out.println("Toast Message:" + toastMessage);
		if (toastMessage.contains("Kavitha")) {
			System.out.println("Parent Record added successfully");
		} else {
			System.out.println("Parent Record not added successfully");
		}
		// 14) Select Today date in Earliest Start Permitted and Nearest Current Time
		Date date = new Date();
		System.out.println("date:" + date);
		DateFormat sdf = new SimpleDateFormat("dd");
		String today = sdf.format(date);
		System.out.println("today:" + today);
		DateFormat sdf2 = new SimpleDateFormat("hh:mm aa");
		date = DateUtils.round(date, Calendar.HOUR);
		String time = sdf2.format(date);
		System.out.println("Current time in AM/PM format:" + time);
		String uCTime = time.toUpperCase();
		System.out.println("Nearest hour:   " + date);
		System.out.println("Current time in AM/PM format:" + uCTime);

		// Thread.sleep(7000);
		// WebElement datePickerElement =
		// driver.findElement(By.xpath("(//a[@class='datePicker-openIcon
		// display'])[1]"));
		// wait.until(ExpectedConditions.visibilityOf(datePickerElement));
		Thread.sleep(7000);
		driver.findElement(By.xpath("(//a[@class='datePicker-openIcon display'])[1]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[contains(text(),'" + today + "')]")).click();
		Thread.sleep(5000);
		// WebElement timeElement =
		// driver.findElement(By.xpath("(//label[text()='Time']/following-sibling::input)[1]"));
		// wait.until(ExpectedConditions.visibilityOf(timeElement));
		driver.findElement(By.xpath("(//label[text()='Time']/following-sibling::input)[1]")).click();
		Thread.sleep(10000);
		driver.findElement(By.xpath("//li[contains(text(),'" + uCTime + "')]")).click();

		// 15) Select 5+ days from Today's date as Due Date
		// WebElement dueDateElement =
		// driver.findElement(By.xpath("(//a[@class='datePicker-openIcon
		// display'])[2]"));
		// wait.until(ExpectedConditions.visibilityOf(dueDateElement));
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//a[@class='datePicker-openIcon display'])[2]")).click();
		Thread.sleep(5000);
		int dueDate = Integer.parseInt(today) + 5;
		System.out.println("dueDate:" + dueDate);
		driver.findElement(By.xpath("//span[contains(text(),'" + dueDate + "')]")).click();

		// 16) click on save
		driver.findElement(By.xpath("(//span[text()='Save'])[2]")).click();
		// 17) Get the Appointment Number
		String appointmentNumberText = driver.findElement(By.xpath(
				"//div[@class='slds-page-header__title slds-m-right--small slds-align-middle clip-text slds-line-clamp']/span"))
				.getText();
		System.out.println("appointmentNumberText:" + appointmentNumberText);
		// 18.Click on the App Launcher Icon left to Setup
		driver.findElement(
				By.xpath("//button[@data-aura-class='forceHeaderButton salesforceIdentityAppLauncherHeader']")).click();
		// 19. Click on View All
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		// 20. Click on Service Appointments
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']"))
				.sendKeys("Service appointments");
		driver.findElement(By.xpath("//p[@class='slds-truncate']/mark")).click();
		// 21. Search the Appoinment Number and Verify
		System.out.println("appointmentNumberText:" + appointmentNumberText);
		driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).sendKeys(appointmentNumberText,
				Keys.ENTER);
		// Expected Result:The Appointment number should be verified
		String expectedResult = driver.findElement(By.xpath("//table//tr/th//a[@data-aura-class='forceOutputLookup']"))
				.getText();
		System.out.println("expectedResult:" + expectedResult);
		if (expectedResult.equalsIgnoreCase(appointmentNumberText)) {
			System.out.println("Success");
		} else {
			System.out.println("Not yet succeeded");
		}

	}
}
