package week2BootcampPractise;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * 
 * @author Tridev Kavitha Bala
 *
 *         1. Login to https://login.salesforce.com/?locale=in 2. Click on menu
 *         button from the Left corner 3. Click view All and click Service
 *         Console from App Launcher 4. Click on the drop down and select
 *         Refunds 5.Click on drop down near Recently viewed and change into
 *         'All'. 6. Click on Chart icon under New Button 7. Click New Chart 8.
 *         Give Chart Name and Select Chart Type 9. Select Aggregate Type as
 *         Average and ggregate Field as Amount 10. Select Grouping Field as
 *         Account and click Save 11. Click on settings icon and change the
 *         Chart Type Expected Result:The Chart type should be changed Step
 *         Video:https://drive.google.com/file/d/1ZDPuVjYyNRcFii-JFzlkU7ofSinp3qvv/view?usp=sharing
 */
public class CreateNewChart {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		// Login to https://login.salesforce.com/?locale=in
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		// 2. Click on menu button from the Left corner
		// 3. Click view All and click Service Console from App Launcher
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		// Click on Service Console
		WebElement searchElement = driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']"));
		wait.until(ExpectedConditions.visibilityOf(searchElement));
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Service Console");
		driver.findElement(By.xpath("//p[@class='slds-truncate']/mark")).click();
		// 4. Click on the drop down and select Refunds
		driver.findElement(By.xpath(
				"//button[@class='slds-button slds-button_icon slds-p-horizontal__xxx-small slds-button_icon-small slds-button_icon-container']/lightning-primitive-icon"))
				.click();
		driver.findElement(By.xpath("//span[text()='Refunds']")).click();
		// 5.Click on drop down near Recently viewed and change into 'All'.
		driver.findElement(By.xpath("(//button[@title='Select a List View'])[1]")).click();
		driver.findElement(By.xpath("//span[text()='All']")).click();
		// 6. Click on Chart icon under New Button
		WebElement chartIconElement = driver.findElement(By.xpath("//button[@title='Show charts']/lightning-primitive-icon"));
		wait.until(ExpectedConditions.visibilityOf(chartIconElement));
		driver.findElement(By.xpath("//button[@title='Show charts']/lightning-primitive-icon")).click();
		// 7. Click New Chart
		WebElement settingsElement = driver.findElement(By.xpath("//a[@title='Settings']//lightning-primitive-icon"));
		wait.until(ExpectedConditions.elementToBeClickable(settingsElement));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click()", settingsElement);
		driver.findElement(By.xpath("//a[@title='New Chart']")).click();
		// 8.Give Chart Name and Select Chart Type
		//Thread.sleep(3000);
		WebElement chartNameElement = driver.findElement(By.xpath("//div[@class='slds-form-element__control slds-grow']/input"));
		wait.until(ExpectedConditions.visibilityOf(chartNameElement));
		driver.findElement(By.xpath("//div[@class='slds-form-element__control slds-grow']/input"))
				.sendKeys("Test Chart");
		driver.findElement(By.xpath("(//a[text()='Horizontal Bar Chart'])[2]")).click();
		driver.findElement(By.xpath("(//a[@title='Vertical Bar Chart'])[2]")).click();
		// 9. Select Aggregate Type as Average and ggregate Field as Amount
		WebElement aggregateTypeElement = driver.findElement(By.xpath("(//a[@class='select'])[3]"));
		wait.until(ExpectedConditions.visibilityOf(aggregateTypeElement));
		driver.findElement(By.xpath("(//a[@class='select'])[3]")).click();
		driver.findElement(By.xpath("//a[@title='Average']")).click();
		driver.findElement(By.xpath("(//a[@class='select'])[4]")).click();
		driver.findElement(By.xpath("//a[@title='Amount']")).click();
		// 10. Select Grouping Field as Account and click Save
		driver.findElement(By.xpath("(//a[@class='select'])[5]")).click();
		driver.findElement(By.xpath("//a[@title='Account']")).click();
		driver.findElement(By.xpath("//button[@class='slds-button slds-button--neutral button-brand uiButton--default uiButton--brand uiButton test-chartEditSave']/span")).click();
		// 11. Click on settings icon and change the Chart Type
		//Thread.sleep(3000);
		WebElement settingsElement2 = driver.findElement(By.xpath("//a[@title='Settings']//lightning-primitive-icon"));
		wait.until(ExpectedConditions.visibilityOf(settingsElement2));
		wait.until(ExpectedConditions.elementToBeClickable(settingsElement2));
		JavascriptExecutor js2 = (JavascriptExecutor) driver;
		js2.executeScript("arguments[0].click()", settingsElement2);
		Thread.sleep(7000);
		WebElement chartElement = driver.findElement(By.xpath("(//a[@title='Vertical Bar Chart'])[1]"));
		wait.until(ExpectedConditions.visibilityOf(chartElement));
		wait.until(ExpectedConditions.elementToBeClickable(chartElement));
		driver.findElement(By.xpath("(//a[@title='Vertical Bar Chart'])[1]")).click();
		//Thread.sleep(3000);
		driver.close();
	}

}
