package day1BootcampPractise;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateNewIndividual {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		/**
		 * 1. Login to https://login.salesforce.com
			2. Click on the toggle menu button from the left corner
			3. Click View All and click Individuals from App Launcher
			4. Click on the Dropdown icon in the Individuals tab
			5. Click on New Individual
			6. Enter the Last Name as 'Kumar'
			7.Click save and verify Individuals Name
			Expected result:
			The Individuals is created Successfully
			**/
		WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		ChromeDriver driver = new ChromeDriver(options);
	
		//	1. Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		
		//2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		
		//3. Click View All and click Individuals from App Launcher
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Individuals");
		
		driver.findElement(By.xpath("//p[@class='slds-truncate']/mark")).click();
		//5. Click on New Individual
		driver.findElement(By.xpath("//a[@title='New']")).click();
		//6. Enter the Last Name as 'Kumar'
		driver.findElement(By.xpath("//input[@placeholder='Last Name']")).sendKeys("Kumar");
		//7.Click save and verify Individuals Name
		driver.findElement(By.xpath("//button[@title='Save']")).click();
		
		String actualLastName = driver.findElement(By.xpath("//span[text()='Kumar']")).getText();
		String expectedLastName = "Kumar";
		if(actualLastName.contains(expectedLastName)) {
			System.out.println("Individual Name is correct");
		}else {
			System.out.println("Individual Name is not correct");
		}
	}

}
