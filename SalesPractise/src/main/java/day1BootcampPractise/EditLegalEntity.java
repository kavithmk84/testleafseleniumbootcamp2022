package day1BootcampPractise;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EditLegalEntity {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		/**
		 * 
		 1. Login to https://login.salesforce.com
		 2. Click on the toggle menu button from the left corner
		 3. Click View All and click Legal Entities from App Launcher
		 4. Click on the legal Entities tab 
		 5. Search the Legal Entity 'Salesforce Automation by Your Name'
		 6. Click on the Dropdown icon and Select Edit
		 7.Enter the Company name as 'Tetsleaf'.
		 8. Enter Description as 'SalesForce'.
		 9.Select Status as 'Active'
		10. Click on Save and Verify Status as Active
		Expected Result:
		The Legal Entity is Edited Successfully
		*/
		
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);

		//1.Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		//2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		//3. Click View All and click Legal Entities from App Launcher
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("legal entities");
		driver.findElement(By.xpath("//p[@class='slds-truncate']/mark")).click();
		//4. Click on the legal Entities tab 
		//5. Search the Legal Entity 'Salesforce Automation by Your Name'
		driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).sendKeys("SalesForce Automation by Kavitha M",Keys.ENTER);
		Thread.sleep(3000);
		driver.findElement(By.xpath("(//a[@title='SalesForce Automation by Kavitha M'])[1]")).click();
		//6. Click on the Dropdown icon and Select Edit
		driver.findElement(By.xpath("//a[@title='Show 5 more actions']")).click();
		driver.findElement(By.xpath("//a[@title='Edit']")).click();
		//7.Enter the Company name as 'Tetsleaf'.
		driver.findElement(By.xpath("(//input[@class=' input'])[2]")).clear();
		driver.findElement(By.xpath("(//input[@class=' input'])[2]")).sendKeys("Testleaf");
		//8. Enter Description as 'SalesForce'.
		driver.findElement(By.xpath("(//div[@class='uiInput uiInputTextArea uiInput--default uiInput--textarea']/textarea)[2]")).clear();
		driver.findElement(By.xpath("(//div[@class='uiInput uiInputTextArea uiInput--default uiInput--textarea']/textarea)[2]")).sendKeys("SalesForce");
		//9.Select Status as 'Active'
		driver.findElement(By.xpath("(//div[@class='uiPopupTrigger']//div/a)[5]")).click();
		driver.findElement(By.xpath("//a[@title='Active']")).click();
		//10. Click on Save and Verify Status as Active
		driver.findElement(By.xpath("(//span[text()='Save'])[2]")).click();
		String statusValue = driver.findElement(By.xpath("(//span[text()='Save'])[2]")).getText();
		System.out.println("Status Value:"+statusValue);
	}

}
