package day1BootcampPractise;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EditAnIndividual {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		/**
		 * Test Steps:
			1. Login to https://login.salesforce.com
			2. Click on the toggle menu button from the left corner
			3. Click View All and click Individuals from App Launcher 
			4. Click on the Individuals tab 
			5. Search the Individuals 'Kumar'
			6. Click on the Dropdown icon and Select Edit
			7.Select Salutation as 'Mr'
			8.Enter the first name as 'Ganesh'.
			9. Click on Save and Verify the first name as 'Ganesh'
			Expected Result:
			The Individuals is Edited Successfully
		 */
		
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		//	1. Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		//2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		//3. Click View All and click Individuals from App Launcher
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Individuals");
		driver.findElement(By.xpath("//p[@class='slds-truncate']/mark")).click();
		//5. Search the Individuals 'Kumar'
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).sendKeys("Kumar");
		//6. Click on the Dropdown icon and Select Edit
		//driver.findElement(By.xpath("(//div[@class='forceVirtualActionMarker forceVirtualAction']//span/span[1])[1]")).click();
		//driver.findElement(By.xpath("//li[@class='uiMenuItem']/a[@title='Edit']")).click();
		//driver.findElement(By.xpath("//a[@class='slds-button slds-button--icon-x-small slds-button--icon-border-filled']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Kumar')]")).click();
		driver.findElement(By.xpath("//div[@title='Edit']")).click();
		//7.Select Salutation as 'Mr'
		driver.findElement(By.xpath("(//a[@class='select'])[1]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[@title='Mr.']")).click();
		//8.Enter the first name as 'Ganesh'.
		driver.findElement(By.xpath("//input[@placeholder='First Name']")).clear();
		driver.findElement(By.xpath("//input[@placeholder='First Name']")).sendKeys("Ganesh");
		//9. Click on Save and Verify the first name as 'Ganesh'
		driver.findElement(By.xpath("//button[@title='Save']")).click();
		String fullName = driver.findElement(By.xpath("(//span[@class='uiOutputText'])[1]")).getText();
		System.out.println("Full Name:"+fullName);
		String[] str = fullName.split(" ");
		String salutation = str[0];
		String actualFirstName = str[1];
		String actualLastName = str[2];
		System.out.println("Salutation is:"+salutation+"\nFirst Name is:"+actualFirstName+"\nLast Name is:"+actualLastName);
		
		String expectedFirstName = "Ganesh";
		if(actualFirstName.equalsIgnoreCase(expectedFirstName)) {
			System.out.println("First Names are same");
		}else {
			System.out.println("First Names are not same");
		}
	}

}
