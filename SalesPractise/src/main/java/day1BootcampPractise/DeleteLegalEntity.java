package day1BootcampPractise;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DeleteLegalEntity {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		/**Test Steps:
			1. Login to https://login.salesforce.com
			2. Click on the toggle menu button from the left corner
			3. Click View All and click Legal Entities from App Launcher
			4. Click on the legal Entities tab 
			5. Search the Legal Entity 'Salesforce Automation by Your Name'
			6. Click on the Dropdown icon and Select Delete
			7.Click on the Delete option in the displayed popup window.
			8. Verify Whether Legal Entity is Deleted using Legal Entity Name
*/
		
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);

		//1.Login to https://login.salesforce.com
			driver.get("https://login.salesforce.com/");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
			driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
			driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
			driver.findElement(By.id("Login")).click();
			
			//2. Click on the toggle menu button from the left corner
			driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
			//3. Click View All and click Legal Entities from App Launcher
			//Thread.sleep(3000);
			driver.findElement(By.xpath("//button[text()='View All']")).click();
			//Thread.sleep(3000);
			driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("legal entities");
			
			driver.findElement(By.xpath("//p[@class='slds-truncate']/mark")).click();
			
			//5. Search the Legal Entity 'Salesforce Automation by Your Name'
			driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).sendKeys("SalesForce Automation by Kavitha M");
			Thread.sleep(3000);
			driver.findElement(By.xpath("(//a[@title='SalesForce Automation by Kavitha M'])[1]")).click();
			String text = driver.findElement(By.xpath("(//a[@title='SalesForce Automation by Kavitha M'])[1]")).getText();
			System.out.println("text:"+text);
			//6. Click on the Dropdown icon and Select Delete
			driver.findElement(By.xpath("//a[@title='Show 5 more actions']")).click();
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("//a[@title='Delete']")).click();
			//7.Click on the Delete option in the displayed popup window.
			driver.findElement(By.xpath("//span[text()='Delete']")).click();
			//8. Verify Whether Legal Entity is Deleted using Legal Entity Name
			
			 if(text.contains("SalesForce Automation by Kavitha M")) {
				 System.out.println("Entity is not deleted");
			 }else {
				 System.out.println("Entity is deleted");
			 }
	}

}
