package day1BootcampPractise;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateLegalEntity {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/**	1. Login to https://login.salesforce.com
			2. Click on the toggle menu button from the left corner
			3. Click View All and click Legal Entities from App Launcher
			4. Click on the Dropdown icon in the legal Entities tab
			5. Click on New Legal Entity
			6. Enter Name as 'Salesforce Automation by Your Name'
			7.Click save and verify Legal Entity Name
			Expected Result:
			The Legal Entity is created Successfully*/
		
		WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		ChromeDriver driver = new ChromeDriver(options);
	
		//	1. Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		
		//2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		//3. Click View All and click Legal Entities from App Launcher
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("legal entities");
		
		driver.findElement(By.xpath("//p[@class='slds-truncate']/mark")).click();
		
		//5. Click on New Legal Entity
		//driver.findElement(By.xpath("//svg[@data-key='chevrondown']//path")).click();
		driver.findElement(By.xpath("//div[text()='New']")).click();
		//6. Enter Name as 'Salesforce Automation by Your Name'
		driver.findElement(By.xpath("//div[@class='uiInput uiInputText uiInput--default uiInput--input']/input")).sendKeys("SalesForce Automation by Kavitha M");
	
		driver.findElement(By.xpath("//button[@class='slds-button slds-button--neutral uiButton--brand uiButton forceActionButton']")).click();
	
		String expectedResult = driver.findElement(By.xpath("//span[text()='SalesForce Automation by Kavitha M']")).getText();
		String actualResult = "SalesForce Automation by Kavitha M";
		if(expectedResult.equals(actualResult)) {
			System.out.println("Legal entity is created successfully");
		}else {
			System.out.println("Legal entity is not created successfully");
		}
	}

}
