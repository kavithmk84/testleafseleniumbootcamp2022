package day1BootcampPractise;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DeleteAnIndividual {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		/**
		 * 
		1. Login to https://login.salesforce.com
		2. Click on the toggle menu button from the left corner
		3. Click View All and click Individuals from App Launcher
		4. Click on the Individuals tab 
		5. Search the Individuals 'Kumar'
		6. Click on the Dropdown icon and Select Delete
		7.Click on the Delete option in the displayed popup window.
		8. Verify Whether Individual is Deleted using Individual last name
		Expected result:
		The Individual is deleted Successfully 
		*/
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		//	1. Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		//2. Click on the toggle menu button from the left corner
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		//3. Click View All and click Individuals from App Launcher
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Individuals");
		driver.findElement(By.xpath("//p[@class='slds-truncate']/mark")).click();
		//5. Search the Individuals 'Kumar'
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).sendKeys("Kumar");
		//6. Click on the Dropdown icon and Select Delete
		//driver.findElement(By.xpath("(//div[@class='forceVirtualActionMarker forceVirtualAction']//span/span[1])[1]")).click();
		//driver.findElement(By.xpath("//li[@class='uiMenuItem']/a[@title='Edit']")).click();
		//driver.findElement(By.xpath("//a[@class='slds-button slds-button--icon-x-small slds-button--icon-border-filled']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[contains(text(),'Kumar')]")).click();
		driver.findElement(By.xpath("//div[text()='Delete']")).click();
		//7.Click on the Delete option in the displayed popup window.
		driver.findElement(By.xpath("//span[text()='Delete']")).click();
		//8. Verify Whether Individual is Deleted using Individual last name
		String toast = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText();
		System.out.println("Toast Message:"+toast);
		if(toast.contains("Kumar")) {
			System.out.println("Delete implemented succssfully");
		}else {
			System.out.println("Delete was not implemented successfully");
		}
		
	}

}

